## @file
#  Describes the entire platform configuration.
#
#  Copyright (c) 2021 - 2022, ARM Limited. All rights reserved.<BR>
#
#  SPDX-License-Identifier: BSD-2-Clause-Patent
##

[Defines]
  DEC_SPECIFICATION              = 0x0001001A
  PACKAGE_NAME                   = Morello
  PACKAGE_GUID                   = F09A2C11-7864-4B1D-869E-03EE2AD79288
  PACKAGE_VERSION                = 0.1

################################################################################
#
# Include Section - list of Include Paths that are provided by this package.
#                   Comments are used for Keywords and Module Types.
#
################################################################################
[Includes.common]
  Include                        # Root include for the package

[Guids.common]
  # ARM Morello Platform Info descriptor
  gArmMorelloPlatformInfoDescriptorGuid = { 0x891422EF, 0x5377, 0x459E, { 0xA5, 0x42, 0x7A, 0xA2, 0x1A, 0x55, 0x54, 0x7F } }
  gArmMorelloTokenSpaceGuid =  { 0x0A8C3A78, 0xA56F, 0x4788, { 0x83, 0xB4, 0xCD, 0x29, 0x62, 0x96, 0x77, 0x51 } }

  gTda19988Guid   = { 0x4D5D3DF1, 0x9BF5, 0x4F40, { 0x84, 0xDD, 0x7F, 0xE5, 0x11, 0x26, 0x9D, 0x6F } }

[Protocols.common]
  gCadenceI2cGuid = { 0xFD1D4369, 0x5B63, 0x42E8, { 0x9B, 0x6B, 0x44, 0xC5, 0x30, 0x17, 0x50, 0x77 } }
  gTda19988ProtocolGuid = { 0x856879BA, 0xE085, 0x4A08, { 0xAE, 0x6C, 0xA9, 0x0E, 0x46, 0xAF, 0x84, 0xE4 } }

  # The TDA 19988 to be used with the platform GOP
  gArmMorelloPlatformGopTda19988Guid = { 0x2A1F2BF2, 0xAF6B, 0x4EAE, { 0x9E, 0xE6, 0xDC, 0xC5, 0x8A, 0x1A, 0x3A, 0xFA } }

[PcdsFixedAtBuild]
  # Ramdisk
  gArmMorelloTokenSpaceGuid.PcdRamDiskBase|0x88000000|UINT32|0x00000001
  gArmMorelloTokenSpaceGuid.PcdRamDiskSize|0x18000000|UINT32|0x00000002

  # Secondary DDR memory
  gArmMorelloTokenSpaceGuid.PcdDramBlock2Base|0|UINT64|0x00000003

  # Virtio Block device
  gArmMorelloTokenSpaceGuid.PcdVirtioBlkBaseAddress|0x00000000|UINT32|0x00000004
  gArmMorelloTokenSpaceGuid.PcdVirtioBlkSize|0x00000000|UINT32|0x00000005
  gArmMorelloTokenSpaceGuid.PcdVirtioBlkInterrupt|0x00000000|UINT32|0x00000006

  # PCIe
  gArmMorelloTokenSpaceGuid.PcdPciBusMin|0|UINT32|0x00000007
  gArmMorelloTokenSpaceGuid.PcdPciBusMax|255|UINT32|0x00000008
  gArmMorelloTokenSpaceGuid.PcdPciBusCount|256|UINT32|0x00000009
  gArmMorelloTokenSpaceGuid.PcdPciIoBase|0x0|UINT32|0x0000000A
  gArmMorelloTokenSpaceGuid.PcdPciIoSize|0x00400000|UINT32|0x0000000B
  gArmMorelloTokenSpaceGuid.PcdPciIoMaxBase|0x003FFFFF|UINT32|0x0000000C
  gArmMorelloTokenSpaceGuid.PcdPciIoTranslation|0x6F000000|UINT32|0x0000000D
  gArmMorelloTokenSpaceGuid.PcdPciMmio32Base|0x60000000|UINT32|0x0000000E
  gArmMorelloTokenSpaceGuid.PcdPciMmio32Size|0x0F000000|UINT32|0x0000000F
  gArmMorelloTokenSpaceGuid.PcdPciMmio32MaxBase|0x6EFFFFFF|UINT32|0x00000010
  gArmMorelloTokenSpaceGuid.PcdPciMmio32Translation|0x0|UINT32|0x00000011
  gArmMorelloTokenSpaceGuid.PcdPciMmio64Base|0x900000000|UINT64|0x00000012
  gArmMorelloTokenSpaceGuid.PcdPciMmio64Size|0x1FC0000000|UINT64|0x00000013
  gArmMorelloTokenSpaceGuid.PcdPciMmio64MaxBase|0x28BFFFFFFF|UINT64|0x00000014
  gArmMorelloTokenSpaceGuid.PcdPciMmio64Translation|0x0|UINT64|0x00000015
  gArmMorelloTokenSpaceGuid.PcdPciExpressBaseAddress|0x28C0000000|UINT64|0x00000016
  gArmMorelloTokenSpaceGuid.PcdPciConfigurationSpaceSize|0x10000000|UINT64|0x00000017
  gArmMorelloTokenSpaceGuid.PcdPciConfigurationSpaceLimit|0x28CFFFFFFF|UINT64|0x00000018

  # CCIX
  gArmMorelloTokenSpaceGuid.PcdCcixBusMin|0|UINT32|0x0000019
  gArmMorelloTokenSpaceGuid.PcdCcixBusMax|255|UINT32|0x000001A
  gArmMorelloTokenSpaceGuid.PcdCcixBusCount|256|UINT32|0x000001B
  gArmMorelloTokenSpaceGuid.PcdCcixIoBase|0x0|UINT32|0x000001C
  gArmMorelloTokenSpaceGuid.PcdCcixIoSize|0x0400000|UINT32|0x000001D
  gArmMorelloTokenSpaceGuid.PcdCcixIoMaxBase|0x003FFFFF|UINT32|0x000001E
  gArmMorelloTokenSpaceGuid.PcdCcixIoTranslation|0x7F000000|UINT32|0x00000001F
  gArmMorelloTokenSpaceGuid.PcdCcixMmio32Base|0x70000000|UINT32|0x00000020
  gArmMorelloTokenSpaceGuid.PcdCcixMmio32Size|0x0F000000|UINT32|0x00000021
  gArmMorelloTokenSpaceGuid.PcdCcixMmio32MaxBase|0x7EFFFFFF|UINT32|0x000000022
  gArmMorelloTokenSpaceGuid.PcdCcixMmio32Translation|0x0|UINT32|0x00000023
  gArmMorelloTokenSpaceGuid.PcdCcixMmio64Base|0x3000000000|UINT64|0x00000024
  gArmMorelloTokenSpaceGuid.PcdCcixMmio64Size|0x1FC0000000|UINT64|0x00000025
  gArmMorelloTokenSpaceGuid.PcdCcixMmio64MaxBase|0x4FBFFFFFFF|UINT64|0x00000026
  gArmMorelloTokenSpaceGuid.PcdCcixMmio64Translation|0x0|UINT64|0x00000027
  gArmMorelloTokenSpaceGuid.PcdCcixExpressBaseAddress|0x4FC0000000|UINT64|0x0000028
  gArmMorelloTokenSpaceGuid.PcdCcixConfigurationSpaceSize|0x10000000|UINT64|0x00000029
  gArmMorelloTokenSpaceGuid.PcdCcixConfigurationSpaceLimit|0x4FCFFFFFFF|UINT64|0x0000002A

  # Virtio Net device
  gArmMorelloTokenSpaceGuid.PcdVirtioNetBaseAddress|0x00000000|UINT32|0x0000002B
  gArmMorelloTokenSpaceGuid.PcdVirtioNetSize|0x00000000|UINT32|0x0000002C
  gArmMorelloTokenSpaceGuid.PcdVirtioNetInterrupt|0x00000000|UINT32|0x0000002D

  # Base address of Cadence QSPI controller configuration registers
  gArmMorelloTokenSpaceGuid.PcdCadenceQspiDxeRegBaseAddress|0x1C050000|UINT32|0x0000002E

  # EFI Framebuffer
  gArmMorelloTokenSpaceGuid.PcdPlatformGopBufferBase|0x0|UINT64|0x00000032
  gArmMorelloTokenSpaceGuid.PcdPlatformGopBufferSize|0x0|UINT32|0x00000033

  # Base address for the ARM Mali Dxx DPU
  gArmMorelloTokenSpaceGuid.PcdArmMaliDxxBase|0x2CC00000|UINT64|0x00000034
  gArmMorelloTokenSpaceGuid.PcdArmMaliDxxSize|0x00300000|UINT32|0x00000035

  # Hdmi I2C bus
  gArmMorelloTokenSpaceGuid.PcdHdmiI2cBusCadanceControllerInputClk|0x00000000|UINT32|0x00000038
  gArmMorelloTokenSpaceGuid.PcdHdmiI2cBusCadanceControllerIoBase|0x00000000|UINT32|0x00000036
  gArmMorelloTokenSpaceGuid.PcdHdmiI2cBusCadanceControllerIoSize|0x00000000|UINT32|0x00000037
  gArmMorelloTokenSpaceGuid.PcdHdmiI2cBusDeviceAddrTda19988Cec|0x00000000|UINT16|0x0000003A
  gArmMorelloTokenSpaceGuid.PcdHdmiI2cBusDeviceAddrTda19988Hdmi|0x00000000|UINT16|0x0000003B
  gArmMorelloTokenSpaceGuid.PcdHdmiI2cBusSpeed|0x00000000|UINT32|0x00000039

[PcdsFeatureFlag.common]
  gArmMorelloTokenSpaceGuid.PcdRamDiskSupported|FALSE|BOOLEAN|0x0000002F
  gArmMorelloTokenSpaceGuid.PcdVirtioBlkSupported|FALSE|BOOLEAN|0x00000030
  gArmMorelloTokenSpaceGuid.PcdVirtioNetSupported|FALSE|BOOLEAN|0x00000031

[Ppis]
  gNtFwConfigDtInfoPpiGuid =  { 0x8E289A83, 0x44E1, 0x41CF, { 0xA7, 0x41, 0x83, 0x80, 0x89, 0x23, 0x43, 0xA3 } }
